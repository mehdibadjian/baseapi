//  Copyright © 2016 Mehdi Badjian. All rights reserved.

#import <Foundation/Foundation.h>
#import "User.h"

@protocol AuthenticationClient <NSObject>

- (void)loginWithUsername:(NSString*)username
                 password:(NSString*)password
               completion:(void (^)(User*))completion
                  failure:(void (^)(NSError*))failure;

@end

@interface APIClient: NSObject <AuthenticationClient>

@end
