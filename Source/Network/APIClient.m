//  Copyright © 2016 Mehdi Badjian. All rights reserved.

#import "APIClient.h"
#import <AFNetworking/AFNetworking.h>

@implementation APIClient

- (void)loginWithUsername:(NSString *)username
                 password:(NSString *)password
               completion:(void (^)(User *))completion
                  failure:(void (^)(NSError *))failure {
  NSURL* url = [NSURL URLWithString:@"https://example.com"];
  AFHTTPSessionManager* manager = [[AFHTTPSessionManager alloc] initWithBaseURL:url];
  [manager GET:@"login"
    parameters:nil
      progress:nil
       success:^(NSURLSessionDataTask * _Nonnull task, NSDictionary*  _Nullable responseObject) {
         if (completion != nil) {
           User* user = [User new];
           user.firstName = responseObject[@"first_name"];
           user.lastName = responseObject[@"last_name"];
           completion(user);
         }
       }
       failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
       }];
}

@end
