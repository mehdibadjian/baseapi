//
//  User.h
//  BaseAPI
//
//  Created by HB on 24/06/2016.
//  Copyright © 2016 Mehdi Badjian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (nonatomic, strong) NSString* firstName;
@property (nonatomic, strong) NSString* lastName;

@end
