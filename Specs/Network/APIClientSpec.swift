//  Copyright © 2016 Mehdi Badjian. All rights reserved.

import Quick
import Nimble
import OHHTTPStubs

class APIClientSpec: QuickSpec {
  override func spec() {
    describe("APIClient") {
      var client: APIClient!
      beforeEach {
        client = APIClient()
      }

      context("login") {
        beforeEach {
          stub(isHost("example.com")) { _ in
            let stubPath = OHPathForFile("get-login.json", self.dynamicType)
            return fixture(stubPath!, headers: ["Content-Type":"application/json"])
          }
        }

        it("should respond to loginWithUsername:password:)") {
          client.loginWithUsername("username",
                                   password: "password",
                                   completion: { _ in },
                                   failure: { _ in })
        }

        it("should return user details on success") {
          var user: AnyObject?
          client.loginWithUsername("username",
                                   password: "password",
                                   completion: { u in user = u },
                                   failure: nil)
          expect(user).toNotEventually(beNil())
        }

        it("user data should be populated correctly") {
          var user: User?
          client.loginWithUsername("username",
                                   password: "password",
                                   completion: { user = $0 },
                                   failure: nil)
          expect(user?.firstName).toEventually(equal("Foo"))
          expect(user?.lastName).toEventually(equal("Bar"))
        }
      }
    }
  }
}
